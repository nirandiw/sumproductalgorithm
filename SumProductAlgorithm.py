from numpy import array, matrix, ones, sum, prod

class Vertex():
    def __init__(self, compat):
        self.compat=compat
    compat=[]
    neighbors=[] #list of numbers
    messageout={} #list of outgoing messages
    messagein=[] #list of incoming messages from neighbours

    def marginal(self):
        a=prod(self.messagein.values(),0)
        a=self.compat*a
        return a/(sum(a))

def compat(i,j):
    #define the edge potential functions
    if i==j:
        return 1.0
    else:
        return 0.5

def f(x):
    #define the singleton potential functions
    if x%2==0:
        return array([0.7, 0.3])
    else:
        return array([0.1, 0.9])

class myV():
    V=[(Vertex(f(x)))for x in range(6)]
    V[0].neighbors= [1, 2];
    V[1].neighbors=[0, 3, 4];
    V[2].neighbors=[0,5];
    V[3].neighbors =[1];
    V[4].neighbors =[1]
    V[5].neighbors= [2];

    for i in range(6):
        for j in V[i].neighbors:
 #           for x in V[i].neighbors:
#                V[i].meesageout=dict(array([1,1]))
            V[i].meesageout=\
                              dict([(x, array([1,1]))\
                                   for x in V[i].neighbors])
            V[i].messagein=\
                             dict([(x, array([1,1]))\
                                   for x in V[i].neighbors])
            print V[i].messageout

    def broadcast(self):
        for i in range(6):
            for j in self.V[i].neighbors:               
                self.V[i].messageout[j]=(array([0,0]))
               # print self.V[i].messageout[j]
                for my_loop in range(2):
                    self.V[i].messageout[j]=\
                    (self.V[i].messageout[j]+
                    self.V[i].compat[my_loop]*
                    array([compat(my_loop,0),
                        compat(my_loop, 1)])*
                    prod([self.V[i].messagein[x][my_loop]
                        for x in self.V[i].neighbors
                        if x!=j]))
                    print self.V[i].messageout[j]

    def recieve(self):
        for i in range(6):
            for j in self.V[i].neighbors:
                self.V[i].messagein[j]= self.V[j].messageout[i]
                print

def main():
    a=myV()
    for k in range(6):
        a.broadcast()
        a.recieve()

    for k in range(6):
        print'Marginal for noce', k, 'is', a.V[k].marginal()

if __name__=='__main__':
    main()
